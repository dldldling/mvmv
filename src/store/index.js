import Vue from 'vue'
import Vuex from 'vuex'
import router from '../router'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    name: localStorage.getItem("name") || "",

    val: "",
    
    useName: "",
    // submitName: ""
  },
  // 修改state
  mutations: {
    handleName(state, obj) {
      state.name = obj.name
      localStorage.setItem("name", obj.name)
    },

    login(state, val) {
      state.useName = val
    }

  },
  actions: {
    login({commit},obj) {
      fetch(`http://admin.raz-kid.cn/api/manage/user/login.do?username=${obj.useName}&password=${obj.submitName}`, {
          method: 'post'
        })
        .then(res => res.json())
        .then(res => {
          if (res.status==0) {
            router.push("/homes")
            commit("login",res.data.username,
            )
          }
        })
    },
  },
  modules: {}
})