import Vue from 'vue'
import VueRouter from 'vue-router'

import header from "../components/header.vue"
import login from "../components/login.vue"

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'header',
    redirect:'/home',
    component: header,
    children:[
      {
        path:'login',
        name:"login",
        component:login,
      },
      {
        path:'home',
        name:"home",
        component:()=>import("../components/home.vue"),
      },
    ]
  },
  {
    path: '/denglu',
    name: 'denglu',
    component: ()=>import("../views/denglu.vue"),
  },  {
    path: '/homes',
    name: 'homes',
    component: ()=>import("../views/homes.vue"),
  },
  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
